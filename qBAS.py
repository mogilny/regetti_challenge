
from abc import *
import numpy as np
import random

# generic distribution

class Distribution():
    # __metaclass__ = ABCMeta
    
    @abstractmethod
    # Returns two matrices: first one with probabilities, second with vectors from the distribution
    def get_distribution_matrices(self):
        return probs, dms

    @abstractmethod
    # Returns two matrices: first one with probabilities, second with vectors not from the distribution
    def get_non_distribution_matrices(self):
        return probs, dms

    @abstractmethod
    # Same as before, with vectors both from and not from the distribition
    def get_mixed_matrices(self):
        return probs, dms



def int_to_bin(an_int, num_digits):
    return ("{0:b}".format(an_int)).zfill(num_digits)

def bin_str_to_vect(bin_str):
    bst = bin_str.replace('0','F').replace('1','T').replace('F','\x00').replace('T','\x01')
    biv = np.fromstring(bst, dtype=np.bool)
    bivi = np.array(biv, dtype=np.int8)
    return bivi



# qBAS distribution

class BAS(Distribution):
    
    def __init__(self, n, m):
        self._n = n
        self._m = m
        self.num_vectors_in_BAS = 2**self._n + 2**self._m - 2
        self.num_vectors = 2**(n*m)

    def get_distribution_matrices(self):
        dms = np.zeros((self.num_vectors_in_BAS, self._n + self._m), dtype=np.int8)
        probs = np.zeros((self.num_vectors_in_BAS), dtype=np.float) + (1. / self.num_vectors_in_BAS)
        for i in range(0, 2**self._n):
            bit_vector = int_to_bin(i, self._n)            
            matrix = np.zeros((self._n, self._m), dtype=np.int8)
            ix = 0
            for x in bit_vector:
                if x == '1':
                    matrix[ix,:] = 1
                ix += 1
            dms[i] = matrix.flatten()
        base = 2**self._n
        for i in range(1, (2**self._m) - 1):
            bit_vector = bin_str_to_vect(int_to_bin(i, self._m))
            matrix = np.zeros((self._n, self._m), dtype=np.int8)
            ix = 0
            for x in bit_vector:
                if x == 1:
                    matrix[:,ix] = 1
                ix += 1
            dms[i-1+base] = matrix.flatten()
        return probs, dms
 
    def get_non_distribution_matrices(self):    
        probs, dms = self.get_distribution_matrices()
        probs = probs * 0.
        num_bits = dms.shape[1]
        for v in dms:
            # a matrix with an odd Hamming distance is never going to be in the BAS dataset
            num_random_bits = random.randint(0, num_bits-1)
            if num_random_bits % 2 == 0:
                num_random_bits -= 1
            if num_random_bits < 0:
                num_random_bits *= -1
            random_bits = np.random.choice(num_bits, num_random_bits)
            for rb in random_bits:
                v[rb] = 1-v[rb]
        return probs, dms
    
    def get_mixed_matrices(self):
        probs1, dms1 = self.get_distribution_matrices()
        probs2, dms2 = self.get_non_distribution_matrices()
        probs, dms = np.hstack((probs1, probs2)), np.vstack((dms1, dms2))
        return probs, dms
            
    
def change_vectors_Q(mat_vectors):     
    q_vectors = np.zeros((mat_vectors.shape[0], 2*mat_vectors.shape[1]))
    rnr = 0
    for v in mat_vectors:
        cln = 0
        for x in v:
            q_vectors[rnr, cln*2] = x          
            q_vectors[rnr, cln*2+1] = 1-x
            cln += 1
        rnr += 1
    return np.transpose(q_vectors)

if __name__ == "__main__":
    bds = BAS(2,2)
    probs, dms = bds.get_distribution_matrices()
    print('From the distribution')
    print(probs)
    print(dms)
    print(change_vectors_Q(dms))
    print('Not from the distribution')
    probs, dms = bds.get_non_distribution_matrices()
    print(probs)
    print(dms)
    print(change_vectors_Q(dms))
    print('Both')
    probs, dms = bds.get_mixed_matrices()
    print(probs)
    print(dms)
    print(change_vectors_Q(dms))
