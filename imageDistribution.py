#
# Transforms an image into a distribution of vectors following this scheme:
# 
# - The image is divided into 2**n x 2**n pixels (using some averaging)
# - The image is transformed into another one that uses 2^c colours (using KMeans). We need c bits to index the color, per pixel. Close indexes should lead to close colors in RGB space
# - Now we index the pixels of the n x n image
# - The value at each index is one that maps to a color index (c bits)
# - This is a distribution over 2**(n+1) values that takes discrete values over c bits
# - In a QC, we will need 2**(n+1) * c qubits
# - Reasonable values are n = 3, c = 1 (b/w)
#

def image_to_distribution(img, n, c):
    return distribution, colormap

def distribution_to_image(distribution, colormap):
    return image

















