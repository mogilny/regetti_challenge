import tensorflow as tf
import math
from qBAS import *

def unitary(v):
    vv = tf.unstack(v)
    xr = vv[0]
    xi = vv[1]
    yr = vv[2]
    yi = vv[3]
    theta = tf.Variable(random.random())
    t1r = tf.cos(theta)*xr + tf.sin(theta)*xi
    t1i = -1*tf.sin(theta)*xr + tf.cos(theta)*xi
    t2r = tf.cos(theta)*yr - tf.sin(theta)*yi
    t2i = tf.sin(theta)*yr + tf.cos(theta)*yi
    xr = t1r
    xi = t1i
    yr = t2r
    yi = t2i
    t1r = xr*tf.cos(theta) + yi*tf.sin(theta)
    t1i = xi*tf.cos(theta) - yr*tf.sin(theta)
    t2r = yr*tf.cos(theta) + xi*tf.sin(theta)
    t2i = -1*xr*tf.sin(theta) + yi*tf.cos(theta)
    out = [t1r, t1i, t2r, t2i]
    out = tf.stack(out)
    return out,theta
    
def xx(v1, v2):
    vv1 = tf.unstack(v1)
    vv2 = tf.unstack(v2)
    [x1r, x1i, y1r, y1i] = vv1
    [x2r, x2i, y2r, y2i] = vv2
    #print(x1i.shape)
    g = tf.Variable(random.random())
    x = g
    t1r = x1r*tf.cos(x) + y2i*tf.sin(x)
    t1i = x1i*tf.cos(x) - y2r*tf.sin(x)
    t2r = y1r*tf.cos(x) + x2i*tf.sin(x)
    t2i = y1i*tf.cos(x) - x2r*tf.sin(x)
    t3r = x2r*tf.cos(x) + y1i*tf.sin(x)
    t3i = x2i*tf.cos(x) - y1r*tf.sin(x)
    t4r = y2r*tf.cos(x) + x1i*tf.sin(x)
    t4i = y2i*tf.cos(x) - x1r*tf.sin(x)
    out1 = [t1r, t1i, t2r, t2i] 
    out2 = [t3r, t3i, t4r, t4i]
    out1 = tf.stack(out1)
    out2 = tf.stack(out2)
    return [out1, out2]

def c_mult(v,final=False):
    [xr,xi,yr,yi] = v
    tr = xr*yr - xi*yi
    ti = xi*yr + yi*xr
    if final:
        return tr*tr + ti*ti
    return [tr, ti]

def i_compose(vv, n_qbits):
    rl = []
    for i in range(2**n_qbits):
        bs = bin_str_to_vect(int_to_bin(i,n_qbits))
        t_r = None
        t_i = None
        for x in range(n_qbits):
            if bs[x] == 0:
                # a * |0>
                n_r = vv[x*4]
                n_i = vv[x*4+1]
            else:
                # b * |1>
                n_r = vv[x*4+2]
                n_i = vv[x*4+3]
            if t_r is None:
                t_r = n_r
                t_i = n_i
            else:
                t_r = t_r*n_r - t_i*n_i
                t_i = t_r*n_i + t_i*n_r
        magnitude = t_r**2 + t_i**2
        #print(bs)
        #print(magnitude)
        rl.append(magnitude)
    return rl

def compose(v):
    vv = tf.unstack(v)
    [x1r,x1i, y1r,y1i,x2r,x2i, y2r,y2i,x3r,x3i, y3r,y3i,x4r,x4i, y4r,y4i] = vv
    #t0000 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[x3r,x3i])+[x4r,x4i],final=True)
    #t0001 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[x3r,x3i])+[y4r,y4i],final=True)
    #t0010 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[y3r,y3i])+[x4r,x4i],final=True)
    #t0011 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[y3r,y3i])+[y4r,y4i],final=True)
    #t0100 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[x3r,x3i])+[x4r,x4i],final=True)
    #t0101 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[x3r,x3i])+[y4r,y4i],final=True)
    #t0110 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[y3r,y3i])+[x4r,x4i],final=True)
    #t0111 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[y3r,y3i])+[y4r,y4i],final=True)
    #t1000 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[x3r,x3i])+[x4r,x4i],final=True)
    #t1001 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[x3r,x3i])+[y4r,y4i],final=True)
    #t1010 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[y3r,y3i])+[x4r,x4i],final=True)
    #t1011 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[y3r,y3i])+[y4r,y4i],final=True)
    #t1100 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[x3r,x3i])+[x4r,x4i],final=True)
    #t1101 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[x3r,x3i])+[y4r,y4i],final=True)
    #t1110 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[y3r,y3i])+[x4r,x4i],final=True)
    #t1111 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[y3r,y3i])+[y4r,y4i],final=True)
    #return [t0000, t0001, t0010, t0011, t0100, t1010, t0110, t0111, t1000, t1001, t1010, t1011, t1100, t1101, t1110, t1111]
    rl = i_compose(vv, 4)
    return rl
        

import numpy as np

def request_binary():
    #s = [[0 0 0 0], [0 0 1 1],[1 1 0 0],[1 1 1 1],[0 1 0 1],[1 0 1 0]]
    s = [[1]+[0]*15, [0]*3+[1]+[0]*12, [0]*15 + [1], [0]*12+[1]+[0]*3, [0]*5+[1]+[0]*10, [0]*8+[1]+[0]*7]
    s = np.swapaxes(np.array(s), 0, 1)
    return s

def request_binary_not():
    #s = [[0 0 0 0], [0 0 1 1],[1 1 0 0],[1 1 1 1],[0 1 0 1],[1 0 1 0]]
    s = [[0]+[1]+[0]*14, [0]*4+[1]+[0]*11, [0]*14 + [1]+[0], [0]*13+[1]+[0]*2, [0]*4+[1]+[0]*11, [0]*7+[1]+[0]*8]
    s = np.swapaxes(np.array(s), 0, 1)
    return s

def dual_gate(v1,v2,v3,v4):
    out1,theta = unitary(v1)
    out2,theta2 = unitary(v2)
    out3,theta3 = unitary(v3)
    out4,theta4 = unitary(v4)
    out1, out2 = xx(out1, out2)
    out3, out4 = xx(out3, out4)
    return [out1,out2,out3,out4,[theta,theta2,theta3,theta4]]

batch_size = 6
s1 = []
s = [[],[],[],[]]
i = 0
while i < batch_size:
    s[0].append(1.0)
    s[1].append(0.0)
    s[2].append(0.0)
    s[3].append(0.0)
    i += 1
print(s)
v1 = tf.constant(s)
print(v1.shape)
v2 = v1
v3 = v1
v3 = v1
v4 = v1
out1,y = unitary(v1)
out2,y = unitary(v2)
out3,y = unitary(v3)
out4,y = unitary(v4)
#o1 = out1
#o2 = out2
#o3 = out3
#o4 = out4
out1, out2 = xx(out1, out2)
out3, out4 = xx(out3, out4)
#out1,out3,out2,out4,tt = dual_gate(out1,out3,out2,out4)
#out1,out4,out3,out2,tt2 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2,tt3 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2,tt4 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2,tt5 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2,tt6 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2,tt7 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2,tt8 = dual_gate(out1,out4,out3,out2)
q = tf.stack(tf.unstack(out1) + tf.unstack(out2) + tf.unstack(out3) + tf.unstack(out4))

#qp = q
q = compose(q)
#print(qp.shape)
#q = tf.reduce_sum(tf.reshape(q*q, [8, 2, batch_size]), 1)
#qz = q
#print(len(q))
q = tf.stack(q)
qz = q

correct = tf.placeholder(tf.float32, shape=(16, batch_size))
cq = tf.reduce_sum(correct*q, axis=0)
cross_entropy_yes = -1*tf.log(tf.clip_by_value(cq,1e-10,10.0))

incorrect = tf.placeholder(tf.float32, shape=(16, batch_size))
cqi = tf.reduce_sum(incorrect*q, axis=0)
cross_entropy_no = 1*tf.log(tf.clip_by_value(cqi,1e-10,10.0))

#print(cross_entropy.shape)
ce = cross_entropy_yes + cross_entropy_no
cross_entropy = tf.reduce_sum(ce)

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.00001)
#mse = tf.reduce_sum(tf.losses.mean_squared_error(correct, q))
minimize = optimizer.minimize(cross_entropy)
sess = tf.Session()
ini = tf.global_variables_initializer()

# test of i_compose
#vv = [0,0,1,0,0,0,1,0]
#print(vv)
#print(i_compose(vv, 2))
#a=1/0

sess.run(ini)
bds = BAS(2,2)
probs, dms = bds.get_mixed_matrices()
#print(dms)
i = 0
while i < len(probs):
    if probs[i] == 0:
        j = 0
        while j < len(dms[i]):
            if dms[i][j] == 1:
                dms[i][j] = 0
            j += 1
    i += 1
print(probs)
#print(dms)
dms = change_vectors_Q(dms)
dms = request_binary()
dms_not = request_binary_not()
print(dms)
print(dms_not)
i = 0
while i < 100000:
    feed = {correct:dms, incorrect:dms_not}
    sess.run(minimize, feed_dict=feed)
    if i % 1000 == 0:
        #print(sess.run(g, feed_dict=feed))
        #print(sess.run(o1, feed_dict=feed))
        #print(sess.run(o2, feed_dict=feed))
        #print(sess.run(tt, feed_dict=feed))
        #print(sess.run(tt2, feed_dict=feed))
        #print(sess.run(tt3, feed_dict=feed))
        print("outs")
        print(sess.run(out1, feed_dict=feed))
        print(sess.run(out2, feed_dict=feed))
        print(sess.run(out3, feed_dict=feed))
        print(sess.run(out4, feed_dict=feed))
        print(sess.run(qz, feed_dict=feed))
        #print(sess.run(correct, feed_dict=feed))
        print(sess.run(q, feed_dict=feed))
        print("cq")
        print(sess.run(cq, feed_dict=feed))
        #print(sess.run(t2, feed_dict=feed))
        print("batch: " + str(i) + ", error: " + str(sess.run(cross_entropy, feed_dict=feed)))
        #break
    i += 1

print(len(probs))
print(len(dms))
print(probs)
print(dms)
