import matplotlib.pyplot as plt
import numpy as np

def plot_bar(distribution):
    x = np.arange(distribution.shape[0])
    plt.bar(x, distribution)
    plt.show()
    
if __name__ == "__main__":
    dst = np.random.random([16])
    plot_bar(dst)
    
    
