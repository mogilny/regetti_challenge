
# creates tkiz .tex and compiles it in order to produce the graph

import os

#def XX(d, theta):
    #linputs = {'cnt':2, 'labels':['i0', 'i1']}
    #rinputs = {'cnt':2, 'labels':['o0', 'o1']}
    #XX = e.blackbox(d.unit, d.unit, linputs=linputs, rinputs=rinputs, mainlabel='XX(' + str(theta) + ')')
    #return XX

#def Rx(d, theta):
    #linputs = {'cnt':1, 'labels':['i']}
    #rinputs = {'cnt':1, 'labels':['o']}
    #Rx = e.blackbox(d.unit, d.unit, linputs=linputs, rinputs=rinputs, mainlabel='Rx(' + str(theta) + ')')
    #return Rx

#def qubit(d, vector_value):
    #rinputs = {'cnt':1, 'labels':['o']}
    #qubit = e.blackbox(d.unit, d.unit, rinputs=rinputs, mainlabel='|' + str(vector_value) + '>')
    #return qubit


class QCircuit():
    
    def __init__(self, n_qubits):
        self._n_qubits = n_qubits
        self._tkizs = []
        self.coordinates = []
        node = None
        for i in range(n_qubits):
            if node is None:
                node = self.add_node('input', 'i'+str(i), '|0>')
            else:
                node = self.add_node('input', 'i'+str(i), '|0>', 'below', node)
            self.coordinates.append(node)
        
    def add_node(self, node_type, node_name, text, reference=None, other_node=None): # reference = 'right of' ...
        if reference is None:
            line = 'node [' + node_type + ', name=' + node_name + '] {' + text + '};'
        else:
            line = 'node [' + node_type + ', ' + reference + '=' + other_node + '] (' + str(node_name) + ') {' + text + '};'
        self._tkizs.append(line)
        return node_name
    
    def write(self, file_name):
        header = "\documentclass[tikz,14pt,border=10pt]{standalone} "\ 
        "\usepackage{textcomp} " + "\n" \ 
        "\usetikzlibrary{shapes,arrows} " + "\n" \ 
        "\begin{document} " + "\n" \ 
        "% Definition of blocks: " + "\n" \ 
        "\tikzset{% " + "\n" \ 
        "  block/.style    = {draw, thick, rectangle, minimum height = 3em, " + "\n" \ 
        "  minimum width = 3em}, " + "\n" \ 
        "sum/.style      = {draw, circle, node distance = 2cm}, % Adder " + "\n" \ 
        "input/.style    = {coordinate}, % Input " + "\n" \ 
        "output/.style   = {coordinate} % Output " + "\n" \ 
        "} " + "\n" \ 
        "\begin{tikzpicture}[auto, thick, node distance=2cm, >=triangle 45] " + "\n" \ 
        "\draw " + "\n" 
        footer = "\end{tikzpicture} " + "\n" \ 
        "\end{document} " + "\n" 
        with open(file_name, 'w') as f:
            f.write(header + '\n')
            f.write('\n'.join(self._tkizs))
            f.write(footer)
            f.close()
        os.system('pdflatex ' + file_name)
        
    #def save(self, file_name):
        #self.d.save(file_name)


if __name__ == "__main__":
    
    qc = QCircuit(4)
    for i in range(4):
        qc.add_node('block', 'Rx'+str(i), 'Rx(' + "{:.3f}".format(math.pi*(i+1)/4) + ')')
    #qc.add(XX(qc.d, math.pi/3),[0,1])
    #qc.add(XX(qc.d, math.pi/3),[2,3])
    #for i in range(4):
        #qc.add(Rx(qc.d, math.pi*(i+1)/4),[i])
    #qc.add(XX(qc.d, math.pi/3),[0,2])
    #qc.add(XX(qc.d, math.pi/3),[1,3])
    write(self, 'tkiz_generated.tex')
    #qc.save('q-circuit.jpg')
    
