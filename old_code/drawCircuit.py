import SchemDraw as schem
import SchemDraw.elements as e
import matplotlib.pyplot as plt
import math
#plt.xkcd() if you want it to look funky

def XX(d, theta):
    linputs = {'cnt':2, 'labels':['i0', 'i1']}
    rinputs = {'cnt':2, 'labels':['o0', 'o1']}
    XX = e.blackbox(d.unit, d.unit, linputs=linputs, rinputs=rinputs, mainlabel='XX(' + str(theta) + ')')
    return XX

def Rx(d, theta):
    linputs = {'cnt':1, 'labels':['i']}
    rinputs = {'cnt':1, 'labels':['o']}
    Rx = e.blackbox(d.unit, d.unit, linputs=linputs, rinputs=rinputs, mainlabel='Rx(' + str(theta) + ')')
    return Rx

def qubit(d, vector_value):
    rinputs = {'cnt':1, 'labels':['o']}
    qubit = e.blackbox(d.unit, d.unit, rinputs=rinputs, mainlabel='|' + str(vector_value) + '>')
    return qubit


class QCircuit():
    
    def __init__(self, n_qubits):
        self._n_qubits = n_qubits
        self.d = schem.Drawing()
        self.anchors = []
        for i in range(n_qubits):
            qub = self.d.add(qubit(self.d, '0'), xy=[0, self.d.unit*(i+1)])
            self.anchors.append(qub.o)
        
    def add(self, gate, qubit_input_lines):
        anchors = [self.anchors[i] for i in qubit_input_lines]
        nel = self.d.add(gate, anchors=anchors)
        if len(qubit_input_lines) == 1:
            self.anchors[qubit_input_lines[0]] = nel.o
        if len(qubit_input_lines) == 2:
            self.anchors[qubit_input_lines[0]] = nel.o0
            self.anchors[qubit_input_lines[1]] = nel.o1
    
    def draw(self):
        self.d.draw()
        
    def save(self, file_name):
        self.d.save(file_name)


if __name__ == "__main__":
    
    n_qubits = 4
    d = schem.Drawing()
    anchors = []
    for i in range(n_qubits):
        qub = d.add(qubit(d, '0'), xy=[0, d.unit*(i+1)])
        anchors.append(qub.o)
    for x in range(4):
        rx = d.add(Rx(d, "{:.3f}".format(math.pi*(x+1)/4)), xy=anchors[x], anchors='i')        
        #anchors[x] = rx.o
    d.draw()    
    #qc = QCircuit(4)
    #for i in range(4):
        #qc.add(Rx(qc.d, "{:.3f}".format(math.pi*(i+1)/4)), [i])
    ##qc.add(XX(qc.d, math.pi/3),[0,1])
    #qc.add(XX(qc.d, math.pi/3),[2,3])
    #for i in range(4):
        #qc.add(Rx(qc.d, math.pi*(i+1)/4),[i])
    #qc.add(XX(qc.d, math.pi/3),[0,2])
    #qc.add(XX(qc.d, math.pi/3),[1,3])
    #qc.draw()
    #qc.save('q-circuit.jpg')
    





