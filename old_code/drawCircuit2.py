#import SchemDraw as schem
#import SchemDraw.elements as e
import matplotlib.pyplot as plt
import math
import networkx as nx
#plt.xkcd() if you want it to look funky

#def XX(d, theta):
    #linputs = {'cnt':2, 'labels':['i0', 'i1']}
    #rinputs = {'cnt':2, 'labels':['o0', 'o1']}
    #XX = e.blackbox(d.unit, d.unit, linputs=linputs, rinputs=rinputs, mainlabel='XX(' + str(theta) + ')')
    #return XX

#def Rx(d, theta):
    #linputs = {'cnt':1, 'labels':['i']}
    #rinputs = {'cnt':1, 'labels':['o']}
    #Rx = e.blackbox(d.unit, d.unit, linputs=linputs, rinputs=rinputs, mainlabel='Rx(' + str(theta) + ')')
    #return Rx

#def qubit(d, vector_value):
    #rinputs = {'cnt':1, 'labels':['o']}
    #qubit = e.blackbox(d.unit, d.unit, rinputs=rinputs, mainlabel='|' + str(vector_value) + '>')
    #return qubit


class QCircuit():
    
    def __init__(self, n_qubits):
        self._n_qubits = n_qubits
        self.gr = nx.Graph()
        self.lines = []
        self.current_node = 0
        for i in range(n_qubits):
            self.gr.add_node('|0>')
            self.lines.append(self.current_node)
            self.current_node += 1
        
    def add(self, gate, qubit_input_lines):
        self.gr.add_node(self.current_node)
        self.current_node += 1
        for i in qubit_input_lines:
            self.gr.add_edge(i, self.current_node-1)
        for i in qubit_input_lines:
            self.lines[i] = self.current_node-1        
        
    def draw(self):
        nx.draw_networkx(self.gr)
        
    #def save(self, file_name):
        #self.d.save(file_name)


if __name__ == "__main__":
    
    qc = QCircuit(4)
    for i in range(4):
        qc.add('Rx(' + "{:.3f}".format(math.pi*(i+1)/4) + ')', [i])
    #qc.add(XX(qc.d, math.pi/3),[0,1])
    #qc.add(XX(qc.d, math.pi/3),[2,3])
    #for i in range(4):
        #qc.add(Rx(qc.d, math.pi*(i+1)/4),[i])
    #qc.add(XX(qc.d, math.pi/3),[0,2])
    #qc.add(XX(qc.d, math.pi/3),[1,3])
    qc.draw()
    plt.show()
    #qc.save('q-circuit.jpg')
    
