import tensorflow as tf
import math
from qBAS import *
from plot import *

def unitary(v,end_early=False):
    vv = tf.unstack(v)
    xr = vv[0]
    xi = vv[1]
    yr = vv[2]
    yi = vv[3]
    theta = tf.Variable(0.25,dtype=tf.float64)
    theta2 = tf.Variable(-0.25,dtype=tf.float64)
    if not end_early:
        t1r = tf.cos(theta)*xr + tf.sin(theta)*xi
        t1i = -1*tf.sin(theta)*xr + tf.cos(theta)*xi
        t2r = tf.cos(theta)*yr - tf.sin(theta)*yi
        t2i = tf.sin(theta)*yr + tf.cos(theta)*yi
        xr = t1r
        xi = t1i
        yr = t2r
        yi = t2i
    #if not end_early:
    t1r = xr*tf.cos(theta2) + yi*tf.sin(theta2)
    t1i = xi*tf.cos(theta2) - yr*tf.sin(theta2)
    t2r = yr*tf.cos(theta2) + xi*tf.sin(theta2)
    t2i = -1*xr*tf.sin(theta2) + yi*tf.cos(theta2)
    out = [t1r, t1i, t2r, t2i]
    out = tf.stack(out)
    return out
    
def xx(v1, v2):
    vv1 = tf.unstack(v1)
    vv2 = tf.unstack(v2)
    [x1r, x1i, y1r, y1i] = vv1
    [x2r, x2i, y2r, y2i] = vv2
    #print(x1i.shape)
    g = tf.Variable(0.5,dtype=tf.float64)
    x = g
    #t1r = x1r*tf.cos(x) + y2i*tf.sin(x)
    #t1i = x1i*tf.cos(x) - y2r*tf.sin(x)
    #t2r = y1r*tf.cos(x) + x2i*tf.sin(x)
    #t2i = y1i*tf.cos(x) - x2r*tf.sin(x)
    #t3r = x2r*tf.cos(x) + y1i*tf.sin(x)
    #t3i = x2i*tf.cos(x) - y1r*tf.sin(x)
    #t4r = y2r*tf.cos(x) + x1i*tf.sin(x)
    #t4i = y2i*tf.cos(x) - x1r*tf.sin(x)
    t1r = (x1r+y2i*tf.cos(g)+y2r*tf.sin(g))/math.sqrt(2)
    t1i = (x1i+y2i*tf.sin(g)-y2r*tf.cos(g))/math.sqrt(2)
    t2r = (y1r+x2i)/math.sqrt(2)
    t2i = (y1i-x2r)/math.sqrt(2)
    t3r = (x2r+y1i)/math.sqrt(2)
    t3i = (x2i-y1r)/math.sqrt(2)
    t4r = (y2r+x1i*tf.cos(g)-x1r*tf.sin(g))/math.sqrt(2)
    t4i = (y2i-x1i*tf.sin(g)-x1r*tf.cos(g))/math.sqrt(2)
    out1 = [t1r, t1i, t2r, t2i] 
    out2 = [t3r, t3i, t4r, t4i]
    out1 = tf.stack(out1)
    out2 = tf.stack(out2)
    return [out1, out2,g]

def c_mult(v,final=False):
    [xr,xi,yr,yi] = v
    tr = xr*yr - xi*yi
    ti = xi*yr + yi*xr
    if final:
        return tr*tr + ti*ti
    return [tr, ti]

def compose(v):
    vv = tf.unstack(v)
    [x1r,x1i, y1r,y1i,x2r,x2i, y2r,y2i,x3r,x3i, y3r,y3i,x4r,x4i, y4r,y4i] = vv
    t0000 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[x3r,x3i])+[x4r,x4i],final=True)
    t0001 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[x3r,x3i])+[y4r,y4i],final=True)
    t0010 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[y3r,y3i])+[x4r,x4i],final=True)
    t0011 = c_mult(c_mult(c_mult([x1r,x1i,x2r,x2i])+[y3r,y3i])+[y4r,y4i],final=True)
    t0100 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[x3r,x3i])+[x4r,x4i],final=True)
    t0101 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[x3r,x3i])+[y4r,y4i],final=True)
    t0110 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[y3r,y3i])+[x4r,x4i],final=True)
    t0111 = c_mult(c_mult(c_mult([x1r,x1i,y2r,y2i])+[y3r,y3i])+[y4r,y4i],final=True)
    t1000 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[x3r,x3i])+[x4r,x4i],final=True)
    t1001 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[x3r,x3i])+[y4r,y4i],final=True)
    t1010 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[y3r,y3i])+[x4r,x4i],final=True)
    t1011 = c_mult(c_mult(c_mult([y1r,y1i,x2r,x2i])+[y3r,y3i])+[y4r,y4i],final=True)
    t1100 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[x3r,x3i])+[x4r,x4i],final=True)
    t1101 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[x3r,x3i])+[y4r,y4i],final=True)
    t1110 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[y3r,y3i])+[x4r,x4i],final=True)
    t1111 = c_mult(c_mult(c_mult([y1r,y1i,y2r,y2i])+[y3r,y3i])+[y4r,y4i],final=True)
    return [t0000, t0001, t0010, t0011, t0100, t1010, t0110, t0111, t1000, t1001, t1010, t1011, t1100, t1101, t1110, t1111]

import numpy as np
import random

def random_v():
    return random.random()*0.1 - 0.05
def request_binary():
    #s = [[0 0 0 0], [0 0 1 1],[1 1 0 0],[1 1 1 1],[0 1 0 1],[1 0 1 0]]
    #s = [[0,0,0,0], [0,0,0,1],[0,0,1,0],[0,0,1,1],[0,1,1,0],[0,1,0,1]]
    s = [[1]+[0]*15, [0]*1+[1]+[0]*14, [0]*2 + [1] + [0]*13] #[0]*12+[1]+[0]*3, [0]*5+[1]+[0]*10, [0]*8+[1]+[0]*7]
    #s = [[1]+[0]*15, [0]*3+[1]+[0]*12, [0]*5 + [1] + [0]*10]
    #s = [[random_v()+0.16,0,0,random_v()+0.16,0,random_v()+0.16,0,0,random_v()+0.16,0,0,0,random_v()+0.16,0,0,random_v()+0.16]]
    s = np.swapaxes(np.array(s), 0, 1)
    return s

def dual_gate(v1,v2,v3,v4):
    out1 = unitary(v1)
    out2 = unitary(v2)
    out3 = unitary(v3)
    out4 = unitary(v4,end_early=False)
    #out1, out2 = xx(out1, out2)
    #out3, out4 = xx(out3, out4)
    return [out1,out2,out3,out4]

batch_size = 3
s1 = []
s = [[],[],[],[]]
i = 0
while i < batch_size:
    s[0].append(0.0)
    s[1].append(0.0)
    s[2].append(1.0)
    s[3].append(0.0)
    i += 1
print(s)
v1 = tf.constant(s,dtype=tf.float64)
print(v1.shape)
v2 = v1
v3 = v1
v3 = v1
v4 = v1
out1 = unitary(v1)
out2 = unitary(v2)
out3 = unitary(v3)
out4 = unitary(v4)
o1 = out1
o2 = out2
#o3 = out3
#o4 = out4
out1, out2,g = xx(out1, out2)
out3, out4,t = xx(out3, out4)
out1,out3,out2,out4 = dual_gate(out1,out3,out2,out4)
#out1,out3,out4,out2 = dual_gate(out1,out3,out4,out2)
#out1,out4,out3,out2 = dual_gate(out2,out4,out3,out1)
#out1,out4,out3,out2 = dual_gate(out4,out3,out1,out2)
#out1,out4,out3,out2 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2 = dual_gate(out1,out4,out3,out2)
#out1,out4,out3,out2 = dual_gate(out1,out4,out3,out2)
q = tf.stack(tf.unstack(out1) + tf.unstack(out2) + tf.unstack(out3) + tf.unstack(out4))

qb = q
q = compose(q)
#print(qp.shape)
#q = tf.reduce_sum(tf.reshape(q*q, [8, 2, batch_size]), 1)
#qz = q
#print(len(q))
q = tf.stack(q)
qz = q
correct = tf.placeholder(tf.float64, shape=(16, batch_size))
cross_entropy = -1*correct*tf.log(tf.clip_by_value(q,1e-10,10.0))
#print(cross_entropy.shape)
ce = cross_entropy
cross_entropy = tf.reduce_sum(cross_entropy)

optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
mse = tf.reduce_sum(tf.losses.mean_squared_error(correct, q))
minimize = optimizer.minimize(mse)
sess = tf.Session()
ini = tf.global_variables_initializer()

sess.run(ini)
bds = BAS(2,2)
probs, dms = bds.get_mixed_matrices()
#print(dms)
i = 0
while i < len(probs):
    if probs[i] == 0:
        j = 0
        while j < len(dms[i]):
            if dms[i][j] == 1:
                dms[i][j] = 0
            j += 1
    i += 1
print(probs)
#print(dms)
dms = change_vectors_Q(dms)
dms = request_binary()
print(dms)
i = 0
while i < 100000:
    feed = {correct:dms}
    sess.run(minimize, feed_dict=feed)
    if i % 10000 == 0 and i != 0:
        print(sess.run(g, feed_dict=feed))
        print(sess.run(t, feed_dict=feed))
        print(sess.run(o1, feed_dict=feed))
        print(sess.run(o2, feed_dict=feed))
        #print(sess.run(tt, feed_dict=feed))
        #print(sess.run(tt2, feed_dict=feed))
        #print(sess.run(tt3, feed_dict=feed))
        print(sess.run(out3, feed_dict=feed))
        print(sess.run(qz, feed_dict=feed))
        print("batch: " + str(i) + ", error: " + str(sess.run(mse, feed_dict=feed)))
        #qzr=sess.run(qz, feed_dict=feed)
        #qzr=qzr[:,0]
        #print(qzr)
        #print(qzr.shape)
        #plot_bar(qzr)
        #break
    i += 1

qzr = sess.run(qz, feed_dict=feed)
qzr=qzr[:,0]
plot_bar(qzr)

ics = np.array([0.333,0.33333,0.3333,0,0,0,0,0,0,0,0,0,0,0,0])
plot_bar(ics)


print(len(probs))
print(len(dms))
print(probs)
print(dms)
print(qzr)